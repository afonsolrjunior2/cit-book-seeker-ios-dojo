//
//  URLSession+Promises.swift
//  Book Seeker
//
//  Created by Afonso Lucas on 05/07/20.
//  Copyright © 2020 Afonso Lucas. All rights reserved.
//

import Foundation
import Promises

enum HTTPError: LocalizedError {
    case invalidResponse
    case invalidStatusCode
    case noData
}

extension URLSession {
    func dataTask(url: URL) -> Promise<Data> {
        return Promise<Data> { [weak self] fulfill, reject in
            guard let self = self else { reject(HTTPError.noData) ; return }
            self.dataTask(with: url) { data, response, error in
                if let error = error {
                    reject(error)
                    return
                }
                guard let response = response as? HTTPURLResponse else {
                    reject(HTTPError.invalidResponse)
                    return
                }
                guard response.statusCode >= 200, response.statusCode < 300 else {
                    reject(HTTPError.invalidStatusCode)
                    return
                }
                guard let data = data else {
                    reject(HTTPError.noData)
                    return
                }
                fulfill(data)
            }.resume()
        }
    }
}
