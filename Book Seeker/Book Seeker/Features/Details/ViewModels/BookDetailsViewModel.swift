//
//  BookDetailsViewModel.swift
//  Book Seeker
//
//  Created by Afonso Lucas on 05/07/20.
//  Copyright © 2020 Afonso Lucas. All rights reserved.
//

import Foundation

class BookDetailsViewModel {
    
    private let book: Book
    
    public var urlString: String {
        get {
           return book.detailsUrlString
        }
    }
    
    public var bookName: String {
        get {
            return book.title
        }
    }
    
    init(book: Book) {
        self.book = book
    }
    
}
