//
//  BookDetailsViewController.swift
//  Book Seeker
//
//  Created by Afonso Lucas on 05/07/20.
//  Copyright © 2020 Afonso Lucas. All rights reserved.
//

import UIKit
import WebKit

class BookDetailsViewController: UIViewController, WKNavigationDelegate, WKUIDelegate {

    private let viewModel: BookDetailsViewModel
    private var webView: WKWebView!
    
    init(viewModel: BookDetailsViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupWebView()
    }
    
    func setupWebView() {
        webView = WKWebView(frame: .zero)
        webView.navigationDelegate = self
        webView.uiDelegate = self
        
        view.addSubview(webView)
        self.title = viewModel.bookName
        
        webView.translatesAutoresizingMaskIntoConstraints = false
        webView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor).isActive = true
        webView.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor).isActive = true
        webView.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor).isActive = true
        webView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        
        guard let url = URL(string: viewModel.urlString) else { return }
        let request = URLRequest(url: url)
        webView.load(request)
    }

}
