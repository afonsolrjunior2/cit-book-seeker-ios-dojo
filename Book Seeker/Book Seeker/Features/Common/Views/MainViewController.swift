//
//  MainViewController.swift
//  Book Seeker
//
//  Created by Afonso Lucas on 05/07/20.
//  Copyright © 2020 Afonso Lucas. All rights reserved.
//

import UIKit

class MainViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let firstViewController = UIViewController()
        firstViewController.tabBarItem = UITabBarItem(title: "Reading now", image: UIImage(systemName: "book.fill"), tag: 0)

        let secondViewController = UIViewController()
        secondViewController.tabBarItem = UITabBarItem(title: "Library", image: UIImage(systemName: "rectangle.grid.1x2.fill"), tag: 1)
        
        let thirdViewController = UIViewController()
        thirdViewController.tabBarItem = UITabBarItem(title: "Book Store", image: UIImage(systemName: "bag.fill"), tag: 2)

        let searchViewController = SearchViewController()
        secondViewController.tabBarItem = UITabBarItem(title: "Search", image: UIImage(systemName: "magnifyingglass"), tag: 3)

        let tabBarList = [firstViewController, secondViewController, thirdViewController, searchViewController]

        viewControllers = tabBarList
        
        selectedIndex = tabBarList.count - 1
    }
}
