//
//  BookResult.swift
//  Book Seeker
//
//  Created by Afonso Lucas on 05/07/20.
//  Copyright © 2020 Afonso Lucas. All rights reserved.
//

import Foundation

struct BookResult: Decodable {
    let resultCount: Int
    let results: [Book]
}
