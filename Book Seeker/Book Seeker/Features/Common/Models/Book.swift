//
//  Book.swift
//  Book Seeker
//
//  Created by Afonso Lucas on 05/07/20.
//  Copyright © 2020 Afonso Lucas. All rights reserved.
//

import Foundation

struct Book: Decodable {
    let image: String
    let title: String
    let author: String
    let detailsUrlString: String
    
    enum CodingKeys: String, CodingKey {
        case image = "artworkUrl60"
        case title = "trackCensoredName"
        case author = "artistName"
        case detailsUrlString = "trackViewUrl"
    }
    
}

/*
 {"artworkUrl60":"https://is1-ssl.mzstatic.com/image/thumb/Publication113/v4/38/77/d2/3877d20b-95c9-af96-9ee4-aaced107fa14/source/60x60bb.jpg", "artworkUrl100":"https://is1-ssl.mzstatic.com/image/thumb/Publication113/v4/38/77/d2/3877d20b-95c9-af96-9ee4-aaced107fa14/source/100x100bb.jpg", "artistViewUrl":"https://books.apple.com/us/artist/apple-inc/405307759?uo=4", "trackCensoredName":"The Swift Programming Language (Swift 5.2)", "fileSizeBytes":4437045, "trackViewUrl":"https://books.apple.com/us/book/the-swift-programming-language-swift-5-2/id881256329?uo=4", "trackId":881256329, "trackName":"The Swift Programming Language (Swift 5.2)", "releaseDate":"2014-06-02T07:00:00Z", "genreIds":["10023", "38", "9027"], "formattedPrice":"Free", "artistIds":[405307759], "currency":"USD", "kind":"ebook", "artistId":405307759, "artistName":"Apple Inc.", "genres":["Programming", "Books", "Computers & Internet"], "price":0.00,
 "description":"Swift is a programming language for creating iOS, macOS, watchOS, and tvOS apps. Swift builds on the best of C and Objective-C, without the constraints of C compatibility. Swift adopts safe programming patterns and adds modern features to make programming easier, more flexible, and more fun. Swift’s clean slate, backed by the mature and much-loved Cocoa and Cocoa Touch frameworks, is an opportunity to reimagine how software development works.<br /><br />\nThis book provides:<br />\n- A tour of the language.<br />\n- A detailed guide delving into each language feature.<br />\n- A formal reference for the language.", "averageUserRating":4.5, "userRatingCount":1633}

 */
