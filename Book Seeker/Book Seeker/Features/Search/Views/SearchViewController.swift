//
//  SearchViewController.swift
//  Book Seeker
//
//  Created by Afonso Lucas on 05/07/20.
//  Copyright © 2020 Afonso Lucas. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController {

    let titleLabel = UILabel()
    let searchBar = UISearchBar()
    let stackView = UIStackView()
    
    let searchViewModel = SearchViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateStackViewInfo()
    }
    
    private func setupUI() {
        self.view.backgroundColor = .white
        setupTitleLabel()
        setupSeachBar()
        setupStackView()
    }
    
    private func setupTitleLabel() {
        self.view.addSubview(titleLabel)
        self.titleLabel.text = "Search"
        self.titleLabel.font = UIFont.boldSystemFont(ofSize: 36)
        
        self.titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 88).isActive = true
        titleLabel.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor).isActive = true
        titleLabel.trailingAnchor.constraint(lessThanOrEqualTo: self.view.safeAreaLayoutGuide.trailingAnchor, constant: UIScreen.main.bounds.width / 3).isActive = true
    }
    
    private func setupSeachBar() {
        searchBar.delegate = self
        self.view.addSubview(searchBar)

        self.searchBar.translatesAutoresizingMaskIntoConstraints = false
        searchBar.topAnchor.constraint(equalTo: self.titleLabel.bottomAnchor, constant: 22).isActive = true
        searchBar.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor).isActive = true
        searchBar.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor).isActive = true
        searchBar.heightAnchor.constraint(equalToConstant: 44).isActive = true
    }
    
    private func setupStackView() {
        
        self.view.addSubview(stackView)
        
        stackView.axis = .vertical
        stackView.distribution = .fill
        stackView.alignment = .fill
        stackView.spacing = 16

        self.stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.topAnchor.constraint(greaterThanOrEqualTo: self.searchBar.bottomAnchor, constant: 22).isActive = true
        stackView.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor).isActive = true
        stackView.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor).isActive = true
        if let tabBarHeight = self.tabBarController?.tabBar.bounds.height {
            stackView.bottomAnchor.constraint(greaterThanOrEqualTo: self.view.safeAreaLayoutGuide.bottomAnchor, constant: tabBarHeight).isActive = true
        } else {
            stackView.bottomAnchor.constraint(greaterThanOrEqualTo: self.view.safeAreaLayoutGuide.bottomAnchor, constant: 8).isActive = true
        }
        
    }
    
    private func updateStackViewInfo() {
        let searchs = self.searchViewModel.getSearchs()
        stackView.arrangedSubviews.forEach { (arrangedSubview) in
            stackView.removeArrangedSubview(arrangedSubview)
        }
        searchs.forEach { search in
            let label = UILabel()
            label.text = search
            stackView.addArrangedSubview(label)
        }
    }

}

extension SearchViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let searchedText = searchBar.text else { return }
        self.view.endEditing(true)
        self.searchViewModel.saveSearch(search: searchedText)
        searchBar.text = ""
        self.navigationController?.pushViewController(BooksListViewController(), animated: true)
    }
}
