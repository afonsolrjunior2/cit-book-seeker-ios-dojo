//
//  SearchViewModel.swift
//  Book Seeker
//
//  Created by Afonso Lucas on 05/07/20.
//  Copyright © 2020 Afonso Lucas. All rights reserved.
//

import Foundation

class SearchViewModel {
    private let bookService = BookService()
    
    public func getSearchs() -> [String] {
        return self.bookService.getRecentBooksSearched()
    }
    
    public func saveSearch(search: String) {
        self.bookService.saveBookSearched(title: search)
    }
    
}
