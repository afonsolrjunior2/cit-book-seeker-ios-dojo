//
//  BooksListViewController.swift
//  Book Seeker
//
//  Created by Afonso Lucas on 05/07/20.
//  Copyright © 2020 Afonso Lucas. All rights reserved.
//

import UIKit
import WebKit

class BooksListViewController: UITableViewController {

    let viewModel = BooksListViewModel()
    var books = [Book]()
    let reuseIdentifier = BookCellViewModel.reuseIdentifier
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.register(BookTableViewCell.self, forCellReuseIdentifier: reuseIdentifier)
        self.setupUI()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return books.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as? BookTableViewCell else { return UITableViewCell() }

        let book = books[indexPath.row]
        let bookCellViewModel = BookCellViewModel(book: book)
        cell.setup(with: bookCellViewModel)

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard indexPath.row < books.count else { return }
        
        let book = books[indexPath.row]
        let bookDetailsViewModel = BookDetailsViewModel(book: book)
        let detailsViewController = BookDetailsViewController(viewModel: bookDetailsViewModel)
        self.navigationController?.pushViewController(detailsViewController, animated: true)
    }
    
    private func setupUI() {
        self.viewModel.getResultsOfSearch { (result) in
            if let error = result.1 {
                print(error)
                return
            }
            
            guard let booksResult = result.0 else { return }
            
            self.books = booksResult
            self.tableView.reloadData()
        }
    }

}
