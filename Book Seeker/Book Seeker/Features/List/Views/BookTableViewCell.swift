//
//  BookTableViewCell.swift
//  Book Seeker
//
//  Created by Afonso Lucas on 05/07/20.
//  Copyright © 2020 Afonso Lucas. All rights reserved.
//

import UIKit

class BookTableViewCell: UITableViewCell {
    
    private var viewModel: BookCellViewModel! {
        didSet {
            textLabel?.text = viewModel.bookName
            detailTextLabel?.text = viewModel.bookAuthor
        }
    }
    
    private var bookImage = UIImageView()
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setup(with viewModel: BookCellViewModel) {
        self.viewModel = viewModel
        setupImage()
    }
    
    func setupImage() {
        self.viewModel.getImageData {[weak self] (result) in
            guard let self = self else { return }
            if let error = result.1 {
                print(error)
                return
            }
            
            guard let imageData = result.0 else { return }
            self.bookImage = UIImageView(image: UIImage(data: imageData))
            
            self.addSubview(self.bookImage)
            self.translatesAutoresizingMaskIntoConstraints = false
            self.bookImage.translatesAutoresizingMaskIntoConstraints = false
            self.bookImage.topAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
            self.bookImage.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
            self.bookImage.trailingAnchor.constraint(greaterThanOrEqualTo: self.trailingAnchor, constant: self.bounds.width * 0.8).isActive = true
            self.bookImage.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        }
    }
}
