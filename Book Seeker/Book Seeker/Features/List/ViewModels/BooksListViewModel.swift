//
//  BooksListViewModel.swift
//  Book Seeker
//
//  Created by Afonso Lucas on 05/07/20.
//  Copyright © 2020 Afonso Lucas. All rights reserved.
//

import Foundation

class BooksListViewModel {
    
    private let bookService = BookService()
    
    public func getResultsOfSearch(completion: @escaping (BookServiceResult) -> Void) {
        self.bookService.getBooks(completion: completion)
    }
    
}
