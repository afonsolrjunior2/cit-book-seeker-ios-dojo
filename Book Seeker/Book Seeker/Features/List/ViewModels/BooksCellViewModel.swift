//
//  BooksCellViewModel.swift
//  Book Seeker
//
//  Created by Afonso Lucas on 05/07/20.
//  Copyright © 2020 Afonso Lucas. All rights reserved.
//

import Foundation

class BookCellViewModel {
    
    private let bookService = BookService()
    private let book: Book
    
    public static let reuseIdentifier = "bookCell"
    
    public var bookName: String {
        get {
            return book.title
        }
    }
    
    public var bookAuthor: String {
        get {
            return book.author
        }
    }
    
    init(book: Book) {
        self.book = book
    }
    
    public func getImageData(completion: @escaping (BookImageResult) -> Void) {
        self.bookService.getBookImageData(with: book.detailsUrlString, completion: completion)
    }
    
}
