//
//  BookService.swift
//  Book Seeker
//
//  Created by Afonso Lucas on 05/07/20.
//  Copyright © 2020 Afonso Lucas. All rights reserved.
//

import Foundation
import Promises

class BookService: BookServiceProtocol {
    
    private let key = "searchs"
    
    func getBooks(completion: @escaping (BookServiceResult) -> Void) {
        guard let url = URL(string: "https://itunes.apple.com/search?term=swift&entity=ibook") else {
            completion((nil, HTTPError.noData))
            return
        }
        
        URLSession.shared.dataTask(url: url).then { data in
            guard let bookResult = try? JSONDecoder().decode(BookResult.self, from: data) else {
                completion((nil, HTTPError.noData))
                return
            }
            completion((bookResult.results, nil))
        }.catch { error in
            if let error = error as? HTTPError {
                completion((nil, error))
            } else {
                completion((nil, HTTPError.invalidResponse))
            }
            
        }
    }
    
    func saveBookSearched(title: String) {
        let userDefaults = UserDefaults.standard
        if var searchs = userDefaults.array(forKey: key) as? [String] {
            searchs.insert(title, at: 0)
            userDefaults.set(searchs, forKey: key)
            userDefaults.synchronize()
        } else {
            var searchs = [String]()
            searchs.insert(title, at: 0)
            if searchs.count > 10 {
                _ = searchs.popLast()
            }
            userDefaults.set(searchs, forKey: key)
            userDefaults.synchronize()
        }
    }
    
    func getRecentBooksSearched() -> [String] {
        let userDefaults = UserDefaults.standard
        if let searchs = userDefaults.array(forKey: key) as? [String] {
            return searchs
        } else {
            let searchs = [String]()
            userDefaults.set(searchs, forKey: key)
            userDefaults.synchronize()
            return searchs
        }
    }
    
    func getBookImageData(with urlString: String, completion: @escaping (BookImageResult) -> Void) {
        guard let url = URL(string: urlString) else {
            completion((nil, HTTPError.noData))
            return
        }
        
        URLSession.shared.dataTask(url: url).then { data in
            completion((data, nil))
        }.catch { error in
            if let error = error as? HTTPError {
                completion((nil, error))
            } else {
                completion((nil, HTTPError.noData))
            }
            
        }
    }
    
}
