//
//  BookServiceProtocol.swift
//  Book Seeker
//
//  Created by Afonso Lucas on 05/07/20.
//  Copyright © 2020 Afonso Lucas. All rights reserved.
//

import Foundation

typealias BookServiceResult = ([Book]?, HTTPError?)
typealias BookImageResult = (Data?, HTTPError?)

protocol BookServiceProtocol {
    func getBooks(completion: @escaping (BookServiceResult) -> Void)
    func saveBookSearched(title: String)
    func getRecentBooksSearched() -> [String]
    func getBookImageData(with urlString: String, completion: @escaping (BookImageResult) -> Void)
}
